﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace InSpire.Data.RuntimeType
{
    public static class RuntimeTypeBuilder
    {
        public static Type CompileRuntimeType(String typeSignature, Type parentType, params Type[] interfaces)
        {
            TypeBuilder typeBuilder = GetTypeBuilder(typeSignature, parentType);

            ConstructorBuilder constructor = typeBuilder.DefineDefaultConstructor(MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.RTSpecialName);

            foreach(var type in interfaces.Where(i => i.IsInterface)) {

                typeBuilder.AddInterfaceImplementation(type);

                foreach (var propertyInfo in GetPublicProperties(type))
                {
                    var parentProperty = (parentType != null) ? parentType.GetProperty(propertyInfo.Name) : null;
                    if (parentProperty == null)
                    {
                        CreateProperty(typeBuilder, propertyInfo);
                    }
                    else
                    {
                        PropertyBuilder propertyBuilder = typeBuilder.DefineProperty(propertyInfo.Name, PropertyAttributes.None, propertyInfo.PropertyType, null);

                        /* clone attributes */

                        foreach(CustomAttributeData attributeData in parentProperty.GetCustomAttributesData()) {

                            CustomAttributeBuilder attributeBuilder = ToAttributeBuilder(attributeData);
                            propertyBuilder.SetCustomAttribute(attributeBuilder);
                        }
                        
                        /* clone getter */

                        if (parentProperty.CanRead)
                        {
                            MethodBuilder getPropMthdBldr =
                                typeBuilder.DefineMethod("get_" + propertyInfo.Name,
                                MethodAttributes.Virtual |
                                MethodAttributes.Public |
                                MethodAttributes.SpecialName |
                                MethodAttributes.HideBySig,
                                propertyInfo.PropertyType,
                                Type.EmptyTypes);

                            ILGenerator getIL = getPropMthdBldr.GetILGenerator();
                            getIL.Emit(OpCodes.Ldarg_0);
                            getIL.Emit(OpCodes.Call, parentProperty.GetGetMethod());
                            getIL.Emit(OpCodes.Ret);

                            propertyBuilder.SetGetMethod(getPropMthdBldr);
                        }

                        /* clone setter */

                        if (parentProperty.CanWrite)
                        {
                            MethodBuilder setPropMthdBldr =
                                typeBuilder.DefineMethod("set_" + propertyInfo.Name,
                                 MethodAttributes.Virtual |
                                  MethodAttributes.Public |
                                  MethodAttributes.SpecialName |
                                  MethodAttributes.HideBySig,
                                  null, new[] { propertyInfo.PropertyType });

                            ILGenerator setIL = setPropMthdBldr.GetILGenerator();
                            setIL.Emit(OpCodes.Ldarg_0);
                            setIL.Emit(OpCodes.Ldarg_1);
                            setIL.Emit(OpCodes.Call, parentProperty.GetSetMethod());
                            setIL.Emit(OpCodes.Ret);

                            propertyBuilder.SetSetMethod(setPropMthdBldr);
                        }
                    }
                }
            }

            Type objectType = typeBuilder.CreateType();

            return objectType;
        }

        private static TypeBuilder GetTypeBuilder(String typeSignature, Type parentType)
        {
            var an = new AssemblyName(typeSignature);
            AssemblyBuilder assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(an, AssemblyBuilderAccess.Run);
            ModuleBuilder moduleBuilder = assemblyBuilder.DefineDynamicModule("MainModule");
            TypeBuilder tb = moduleBuilder.DefineType(typeSignature
                                , TypeAttributes.Public |
                                TypeAttributes.Class |
                                TypeAttributes.AutoClass |
                                TypeAttributes.AnsiClass |
                                TypeAttributes.BeforeFieldInit |
                                TypeAttributes.AutoLayout
                                , parentType);
            return tb;
        }

        private static void CreateProperty(TypeBuilder tb, PropertyInfo prop)
        {
            CreateProperty(tb, prop.Name, prop.PropertyType, prop.CanRead, prop.CanWrite, prop);
        }

        private static void CreateProperty(TypeBuilder tb, string propertyName, Type propertyType, bool propertyGet, bool propertySet, PropertyInfo prop)
        {
            FieldBuilder fieldBuilder = tb.DefineField("_" + propertyName, propertyType, FieldAttributes.Private);

            PropertyBuilder propertyBuilder = tb.DefineProperty(propertyName, PropertyAttributes.None, propertyType, null);
            MethodBuilder getPropMthdBldr =
                tb.DefineMethod("get_" + propertyName,
                MethodAttributes.Virtual |
                MethodAttributes.Public |
                MethodAttributes.SpecialName |
                MethodAttributes.HideBySig,
                propertyType,
                Type.EmptyTypes);

            ILGenerator getIl = getPropMthdBldr.GetILGenerator();

            getIl.Emit(OpCodes.Ldarg_0);
            getIl.Emit(OpCodes.Ldfld, fieldBuilder);
            getIl.Emit(OpCodes.Ret);

            MethodBuilder setPropMthdBldr =
                tb.DefineMethod("set_" + propertyName,
                 MethodAttributes.Virtual |
                  MethodAttributes.Public |
                  MethodAttributes.SpecialName |
                  MethodAttributes.HideBySig,
                  null, new[] { propertyType });

            ILGenerator setIl = setPropMthdBldr.GetILGenerator();
            Label modifyProperty = setIl.DefineLabel();
            Label exitSet = setIl.DefineLabel();

            setIl.MarkLabel(modifyProperty);
            setIl.Emit(OpCodes.Ldarg_0);
            setIl.Emit(OpCodes.Ldarg_1);
            setIl.Emit(OpCodes.Stfld, fieldBuilder);

            setIl.Emit(OpCodes.Nop);
            setIl.MarkLabel(exitSet);
            setIl.Emit(OpCodes.Ret);

            if (propertyGet)
            {
                propertyBuilder.SetGetMethod(getPropMthdBldr);
                tb.DefineMethodOverride(getPropMthdBldr, prop.GetGetMethod());
            }

            if (propertySet)
            {
                propertyBuilder.SetSetMethod(setPropMthdBldr);
                tb.DefineMethodOverride(setPropMthdBldr, prop.GetSetMethod());
            }
        }

        private static PropertyInfo[] GetPublicProperties(this Type type)
        {
            if (type.IsInterface)
            {
                var propertyInfos = new List<PropertyInfo>();

                var considered = new List<Type>();
                var queue = new Queue<Type>();
                considered.Add(type);
                queue.Enqueue(type);
                while (queue.Count > 0)
                {
                    var subType = queue.Dequeue();
                    foreach (var subInterface in subType.GetInterfaces())
                    {
                        if (considered.Contains(subInterface)) continue;

                        considered.Add(subInterface);
                        queue.Enqueue(subInterface);
                    }

                    var typeProperties = subType.GetProperties(
                        BindingFlags.FlattenHierarchy
                        | BindingFlags.Public
                        | BindingFlags.Instance);

                    var newPropertyInfos = typeProperties
                        .Where(x => !propertyInfos.Contains(x));

                    propertyInfos.InsertRange(0, newPropertyInfos);
                }

                return propertyInfos.ToArray();
            }

            return type.GetProperties(BindingFlags.FlattenHierarchy
                | BindingFlags.Public | BindingFlags.Instance);
        }

        private static CustomAttributeBuilder ToAttributeBuilder(this CustomAttributeData data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            var constructorArguments = new List<object>();
            foreach (var ctorArg in data.ConstructorArguments)
            {
                constructorArguments.Add(ctorArg.Value);
            }

            var propertyArguments = new List<PropertyInfo>();
            var propertyArgumentValues = new List<object>();
            var fieldArguments = new List<FieldInfo>();
            var fieldArgumentValues = new List<object>();
            foreach (var namedArg in data.NamedArguments)
            {
                var fi = namedArg.MemberInfo as FieldInfo;
                var pi = namedArg.MemberInfo as PropertyInfo;

                if (fi != null)
                {
                    fieldArguments.Add(fi);
                    fieldArgumentValues.Add(namedArg.TypedValue.Value);
                }
                else if (pi != null)
                {
                    propertyArguments.Add(pi);
                    propertyArgumentValues.Add(namedArg.TypedValue.Value);
                }
            }
            return new CustomAttributeBuilder(
              data.Constructor,
              constructorArguments.ToArray(),
              propertyArguments.ToArray(),
              propertyArgumentValues.ToArray(),
              fieldArguments.ToArray(),
              fieldArgumentValues.ToArray());
        }
    }

    public static class InvocationHelper
    {
        private static T[] ConcatArrays<T>(params T[][] arrays)
        {
            return (from array in arrays
                    from arr in array
                    select arr).ToArray();
        }

        public static object InvokeGenericMethod<T>(this T target, Expression<Func<T, object>> expression, params Type[] typeArguments)
        {
            return InvokeGenericMethodOn<T>(target, expression, new object[] { target }, typeArguments);
        }

        public static object InvokeGenericMethod<T>(this T target, Expression<Func<T, object>> expression, object[] methodArguments, Type[] typeArguments)
        {
            return InvokeGenericMethodOn<T>(target, expression, ConcatArrays<object>(new object[][] { new object [] { target }, methodArguments }), typeArguments);
        }

        public static object InvokeGenericMethodOn<T>(T target, Expression<Func<T, object>> expression, object[] methodArguments, Type[] typeArguments)
        {
            MethodCallExpression methodCall = (MethodCallExpression)expression.Body;
            var methodInfo = methodCall.Method;

            if (methodInfo.GetGenericArguments().Length != typeArguments.Length)
                throw new ArgumentException(
                    string.Format("The method '{0}' has {1} type argument(s) but {2} type argument(s) were passed. The amounts must be equal.",
                    methodInfo.Name,
                    methodInfo.GetGenericArguments().Length,
                    typeArguments.Length));

            var genericMethod = methodInfo.GetGenericMethodDefinition().MakeGenericMethod(typeArguments);

            return genericMethod.Invoke(target, methodArguments);
        }
    }
}
