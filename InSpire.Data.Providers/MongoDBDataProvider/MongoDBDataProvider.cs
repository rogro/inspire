﻿using Composite.Data;
using Composite.Data.Plugins.DataProvider;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ObjectBuilder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Reflection;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using MongoDB.Bson.Serialization;
using InSpire.Data.RuntimeType;
using Composite.Data.Foundation.CodeGeneration;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using Composite.Data.ProcessControlled;

namespace InSpire.Data.Providers.MongoDBDataProvider
{
    [ConfigurationElementType(typeof(MongoDBDataProviderData))]
    public class MongoDBDataProvider : IWritableDataProvider, ILocalizedDataProvider
    {
        private IDictionary<Type, TypeRegistration> _registrations = new Dictionary<Type, TypeRegistration>();
        private DataProviderContext _context;

        private readonly MongoClient _client;

        public MongoDBDataProvider(string connectionString, string defaultDatabase = "test")
        {
            _client = new MongoClient(connectionString);

            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var assembly in assemblies)
            {
                var types = assembly.GetTypes();
                foreach (var type in types)
                {
                    try
                    {
                        MongoDBCollectionAttribute attr = (MongoDBCollectionAttribute)type.GetCustomAttributes(typeof(MongoDBCollectionAttribute), false).FirstOrDefault();
                        if(attr != null)
                        {
                            var interfaceType = type;
                            var serializableType = RuntimeTypeBuilder.CompileRuntimeType(type.FullName + "_MongoObject", typeof(MongoDBSerializable), interfaceType);

                            var classMap = BsonClassMap.LookupClassMap(serializableType);

                            // register class serializer as interfaceType serializer

                            var bsonSerializer = BsonSerializer.LookupSerializer(serializableType);
                            BsonSerializer.RegisterSerializer(interfaceType, bsonSerializer);

                            // register type info

                            TypeRegistration registration = new TypeRegistration
                            {
                                interfaceType = interfaceType,
                                serializableType = serializableType,
                                collectionName = attr.Name ?? interfaceType.FullName.Replace('.', '_'),
                                database = attr.Database ?? defaultDatabase
                            };

                            _registrations.Add(interfaceType, registration);
                        }
                    }
                    catch { }
                }
            }

            // default mappings

            BsonClassMap.RegisterClassMap<EmptyDataClassBase>(cm =>
            {
                cm.AutoMap();
                cm.UnmapProperty("DataSourceId");
            });
         }

        public DataProviderContext Context
        {
            set 
            { 
                _context = value;
            }
        }

        public IEnumerable<Type> GetSupportedInterfaces()
        {
            return _registrations.Keys;
        }

        public IQueryable<T> GetData<T>() where T : class, IData
        {
            var interfaceType = _registrations.Keys.FirstOrDefault(t => t.Equals(typeof(T)) || t.IsAssignableFrom(typeof(T)));
            var typeRegistration = _registrations[interfaceType];

            var collection = _client.GetServer()
                .GetDatabase(typeRegistration.database)
                .GetCollection(interfaceType, typeRegistration.collectionName);

            var queryable = collection.InvokeGenericMethod(c => c.AsQueryable<object>(), typeRegistration.serializableType);

            return (IQueryable<T>)queryable;
        }

        public T GetData<T>(IDataId dataId) where T : class, IData 
        {
            var interfaceType = _registrations.Keys.FirstOrDefault(t => t.Equals(typeof(T)) || t.IsAssignableFrom(typeof(T)));
            var typeRegistration = _registrations[interfaceType];

            var mongoDataId = (MongoDBDataId)dataId;
            var collection = _client.GetServer()
                .GetDatabase(typeRegistration.database)
                .GetCollection(interfaceType, typeRegistration.collectionName);

            return collection.FindOneAs(typeRegistration.serializableType, Query.EQ("_id", mongoDataId.Id)) as T;
        }

        public List<T> AddNew<T>(IEnumerable<T> datas) where T : class, IData
        {
            List<T> resultData = new List<T>();

            foreach(T data in datas) 
            {
                var interfaceType = _registrations.Keys.FirstOrDefault(t => t.Equals(typeof(T)) || t.IsAssignableFrom(typeof(T)));
                var typeRegistration = _registrations[interfaceType];

                var collection = _client.GetServer()
                    .GetDatabase(typeRegistration.database)
                    .GetCollection(interfaceType, typeRegistration.collectionName);

                MongoDBSerializable item = (MongoDBSerializable)Activator.CreateInstance(typeRegistration.serializableType);

                /* clone properties */

                foreach (var prop in data.GetType().GetProperties())
                {
                    if (prop.CanRead) { 
                        
                        var value = prop.GetGetMethod().Invoke(data, null);

                        /* ouch: do not get property by name as the runtime serializable type might have one property twice... 
                         * perhaps we should look at its DeclaringType and use class impl before interface */

                        var setProp = item.GetType().GetProperties().FirstOrDefault(p => p.Name.Equals(prop.Name));
                        if (setProp != null && setProp.CanWrite)
                        {
                            setProp.SetValue(item, value);
                        }
                    }
                }

                var dataId = (Guid)data.GetKeyProperties().FirstOrDefault().GetValue(data);

                item.DataSourceId = _context.CreateDataSourceId(new MongoDBDataId { Id = dataId }, interfaceType);

                collection.Save(item);
                resultData.Add(item as T);
            }

            return resultData;
        }

        public void Update(IEnumerable<IData> datas)
        {
            foreach (IData data in datas)
            {
                var interfaceType = _registrations.Keys.FirstOrDefault(t => t.Equals(data.GetType()) || t.IsAssignableFrom(data.GetType()));
                var typeRegistration = _registrations[interfaceType];

                var collection = _client.GetServer()
                    .GetDatabase(typeRegistration.database)
                    .GetCollection(interfaceType, typeRegistration.collectionName);

                MongoDBSerializable item = (MongoDBSerializable)Activator.CreateInstance(typeRegistration.serializableType);

                foreach (var prop in data.GetType().GetProperties())
                {
                    if (prop.CanRead)
                    {

                        var value = prop.GetGetMethod().Invoke(data, null);

                        /* ouch: do not get property by name as the runtime serializable type might have one property twice... 
                         * perhaps we should look at its DeclaringType and use class impl before interface */

                        var setProp = item.GetType().GetProperties().FirstOrDefault(p => p.Name.Equals(prop.Name));
                        if (setProp != null && setProp.CanWrite)
                        {
                            setProp.SetValue(item, value);
                        }
                    }
                }
                
                collection.Save(item);
            }
        }

        public void Delete(IEnumerable<DataSourceId> dataSourceIds)
        {
            foreach (DataSourceId dataSourceId in dataSourceIds)
            {
                var interfaceType = dataSourceId.InterfaceType;
                var typeRegistration = _registrations[interfaceType];

                var collection = _client.GetServer()
                    .GetDatabase(typeRegistration.database)
                    .GetCollection(typeRegistration.interfaceType, typeRegistration.collectionName);

                MongoDBDataId mongoDataId = (MongoDBDataId)dataSourceId.DataId;

                collection.Remove(Query.EQ("_id", mongoDataId.Id));
            }
        }

        /* localization */

        public void AddLocale(System.Globalization.CultureInfo cultureInfo)
        {
            /* not much to do here... */
        }

        public void RemoveLocale(System.Globalization.CultureInfo cultureInfo)
        {
            /* not much to do here... */     
        }
   
        private class TypeRegistration
        {
            public Type interfaceType { get; set; }
            public Type serializableType { get; set; }
            public String collectionName { get; set; }
            public String database { get; set; }
        }
    }

    [Assembler(typeof(MongoDBDataProviderAssembler))]
    public sealed class MongoDBDataProviderData : DataProviderData
    {
        private const string _connectionStringPropertyName = "connectionString";
        [ConfigurationProperty(_connectionStringPropertyName, IsRequired = true)]
        public string ConnectionString
        {
            get { return (string)base[_connectionStringPropertyName]; }
            set { base[_connectionStringPropertyName] = value; }
        }

        private const string _defaultDatabasePropertyName = "defaultDatabase";
        [ConfigurationProperty(_defaultDatabasePropertyName, IsRequired = false)]
        public string DefaultDatabase
        {
            get { return (string)base[_defaultDatabasePropertyName]; }
            set { base[_defaultDatabasePropertyName] = value; }
        }
    }

    public sealed class MongoDBDataProviderAssembler : IAssembler<IDataProvider, DataProviderData>
    {
        public IDataProvider Assemble(IBuilderContext context, DataProviderData objectConfiguration, IConfigurationSource configurationSource, ConfigurationReflectionCache reflectionCache)
        {
            MongoDBDataProviderData configuration = (MongoDBDataProviderData)objectConfiguration;

            return new MongoDBDataProvider(configuration.ConnectionString, configuration.DefaultDatabase);
        }
    }
}
