﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InSpire.Data.Providers.MongoDBDataProvider
{
    [AttributeUsage(AttributeTargets.Interface, Inherited = true, AllowMultiple = false)]
    public class MongoDBCollectionAttribute : Attribute
    {
        public String Name { get; set; }
        public String Database { get; set; }
        public int Version { get; set; }
    }
}
