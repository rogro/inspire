﻿using Composite.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InSpire.Data.Providers.MongoDBDataProvider
{
    public class MongoDBDataId : IDataId
    {
        public Guid Id { get; set; }
    }
}
