﻿using Composite.Data;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InSpire.Data.Providers.MongoDBDataProvider
{
    public class MongoDBSerializable
    {
        private DataSourceId _dataSourceId;

        [BsonIgnore]
        public DataSourceId DataSourceId
        {
            get 
            {
                if (_dataSourceId == null)
                {
                    var dataId = (Guid)this.GetType().GetProperty("Id").GetValue(this);
                    _dataSourceId = new DataSourceId(new MongoDBDataId { Id = dataId }, typeof(MongoDBDataProvider).Name, GetMongoDBCollectionInterface(GetType()));
                }
                return _dataSourceId;
            }
            set { _dataSourceId = value; }
        }

        private int _version_set = -1;
        public int _version
        {
            get
            {
                if (_version_set == -1) 
                {
                    var attr = GetMongoDBCollectionAttribute(GetType());
                    if (attr != null)
                    {
                        _version_set = attr.Version;
                    }
                }

                return _version_set;
            }
            set
            {
                _version_set = value;
            }
        }

        private int _pub_scope_set = -1;
        public int _pub_scope
        {
            get
            {
                if (_pub_scope_set == -1 && DataSourceId != null)
                {
                    _pub_scope_set = (int)DataSourceId.PublicationScope;
                }
                return _pub_scope_set;
            }
            set { _pub_scope_set = value; }
        }

        private CultureInfo _local_scope_set = null;
        public String _local_scope
        {
            get
            {
                var x = CultureInfo.CurrentCulture;

                if (_local_scope_set == null && DataSourceId != null)
                {
                    _local_scope_set = DataSourceId.LocaleScope;
                }

                return (_local_scope_set == null) ? _local_scope_set.Name : String.Empty;
            }
            set
            {
                try
                {
                    if (!String.IsNullOrEmpty(value))
                    {
                        _local_scope_set = CultureInfo.GetCultureInfo(value);
                    }
                } catch {}
            }
        }

        public static Type GetMongoDBCollectionInterface(Type classType)
        {
            foreach (var typeInterface in classType.GetInterfaces())
            {
                MongoDBCollectionAttribute attr = (MongoDBCollectionAttribute)typeInterface.GetCustomAttributes(typeof(MongoDBCollectionAttribute), true).FirstOrDefault();
                if (attr != null)
                {
                    return typeInterface;
                }
            }

            return null;
        }

        public static MongoDBCollectionAttribute GetMongoDBCollectionAttribute(Type classType)
        {
            foreach (var typeInterface in classType.GetInterfaces())
            {
                MongoDBCollectionAttribute attr = (MongoDBCollectionAttribute)typeInterface.GetCustomAttributes(typeof(MongoDBCollectionAttribute), true).FirstOrDefault();
                if (attr != null)
                {
                    return attr;
                }
            }

            return null;
        }
    }
}
